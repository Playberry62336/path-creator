﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip LevelComplete;
    public AudioClip LevelFail;

    public AudioSource AS;
    public AudioSource BGAS;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void resetMusic()
    {
        int isMusicOn = PlayerPrefs.GetInt("Sound", 1);

        if (isMusicOn == 1) // Music is On
        {
            UnMuteAll();
        }
        if (isMusicOn == 0) // Music is Off
        {
            MuteAll();
        }
    }

    public void OnClickMusicOn() // from button 
    {
        PlayerPrefs.SetInt("Sound", 1);
        PlayerPrefs.Save();
        resetMusic();
    }
    public void OnClickMusicOff() // from button 
    {
        PlayerPrefs.SetInt("Sound", 0);
        PlayerPrefs.Save();
        resetMusic();
    }


    public void MuteAll()
    {
        AS.volume = 0;
        BGAS.volume = 0;
    }
    public void UnMuteAll()
    {
        AS.volume = 1;
        BGAS.volume = 1;
    }

    public void OnLevelComplete()
    {
        AS.PlayOneShot(LevelComplete);
    }
    public void OnLevelFail()
    {
        AS.PlayOneShot(LevelFail);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
