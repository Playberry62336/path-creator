﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public int[] prices;
    public int Current_Player;
    public GameObject[] Buy_Detail;
    public GameObject[] Use_Text;
    public GameObject[] Players;
    public GameObject Start_Button;
    public GameObject Buy_Button;
    public Text coin_Text;

    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("Player" + 0, 1);
        PlayerStatus();
        PlayerPrefs.Save();

        int Amount = PlayerPrefs.GetInt("Coins");
        coin_Text.text = Amount.ToString();

        int i = 0;
        foreach (GameObject x in Players)
        {
              x.SetActive(false);

            if (PlayerPrefs.GetInt("Player" + i, 0) == 1) // is bought
            {

                Players[i].SetActive(true);
                Buy_Detail[i].SetActive(false);
                Use_Text[i].SetActive(true);

            }
            else
            {
                
                Players[i].SetActive(true);
                Buy_Detail[i].SetActive(true);
                Use_Text[i].SetActive(false);
            }



            i++;
        }
        Players[Current_Player].SetActive(true);


    }
    

    // Update is called once per frame
    void Update()
    {
        PlayerPrefs.SetInt("SelectdPlayer", Current_Player);
    }
    public void OnClickPlayer1()
    {
        foreach(GameObject x in Players)
        {
            x.SetActive(false);
        }
        Current_Player = 0;
        Players[Current_Player].SetActive(true);
        PlayerStatus();
    }
    public void OnClickPlayer2()
    {
        foreach(GameObject x in Players)
        {
            x.SetActive(false);
        }
        Current_Player = 1;
        Players[Current_Player].SetActive(true);
        PlayerStatus();
    }
    public void OnClickPlayer3()
    {
        foreach(GameObject x in Players)
        {
            x.SetActive(false);
        }
        Current_Player = 2;
        Players[Current_Player].SetActive(true);
        PlayerStatus();
    }
    //Player 0 Unlocked, 0=false, 1 unlocked
    public void PlayerStatus()
    {
        int Unlocked = PlayerPrefs.GetInt("Player"+Current_Player,0);
        if (Unlocked == 0)
        {
            Start_Button.SetActive(false);
            Buy_Button.SetActive(true);
        }
        else if (Unlocked == 1)
        {
            Start_Button.SetActive(true);
            Buy_Button.SetActive(false);
        }  
    }
    public void OnClickStart()
    {
         
        SceneManager.LoadScene(0);
    }
    public void OnClickBuy()
    {
        int AvailableCoins = PlayerPrefs.GetInt("Coins", 0);

       
       if (AvailableCoins >= prices[Current_Player])
            {
                PlayerPrefs.SetInt("Player" + Current_Player, 1);
                PlayerStatus();
                DeleteCoin(prices[Current_Player]);
            }
        
    }
    public void DeleteCoin(int a)
    {
        int temp = PlayerPrefs.GetInt("Coins", 0);
        temp = temp - a;

        PlayerPrefs.SetInt("Coins", temp);
        PlayerPrefs.Save();
        Button_Setup();
        UpdateCointext();
    }
    public void UpdateCointext()
    {
        int Amount=PlayerPrefs.GetInt("Coins",0);
        coin_Text.text = Amount.ToString();
    }
    public void Button_Setup()
    {
        Buy_Detail[Current_Player].SetActive(false);
        Use_Text[Current_Player].SetActive(true);
    }

}
