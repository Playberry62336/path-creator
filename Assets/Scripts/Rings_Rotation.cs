﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rings_Rotation : MonoBehaviour
{
    public float Rotation_speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RingsRoatation();
    }
    public void RingsRoatation()
    {
        gameObject.transform.Rotate(new Vector3(0, -1, 0) * Rotation_speed * Time.deltaTime);
    }
}
