﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator_Rotation : MonoBehaviour
{
    public float rotator_speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RotatorRoatation();
    }
    public void RotatorRoatation()
    {
        gameObject.transform.Rotate(new Vector3(0, -1, 0) * rotator_speed * Time.deltaTime);
    }
}
