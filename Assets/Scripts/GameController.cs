﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using PathCreation.Examples;

public class GameController : MonoBehaviour
{
    public Ui_Controller UIS;
    public int current_Level;
    public GameObject Music;
    public bool test_Mode;
    public GameObject Player;
    public GameObject[] Levels;
    public SoundManager SM;
    public Slider SliderDistanceCovered;
    public PathFollower PF;
    // Start is called before the first frame update
    void Start()
    {
        
            current_Level = PlayerPrefs.GetInt("Level", 0);
            InitLevel();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            OnStartRun();
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            OnStopRun();
        }

        if (PF != null)
            SliderDistanceCovered.value = PF.distanceTravelled;
    }



    public void OnStartRun()
    {
        Player.GetComponent<PlayerHelper>().OnStartRun();
    }
    public void OnStopRun()
    {
        Player.GetComponent<PlayerHelper>().OnStopRun();
    }
    public void InitLevel()
    {
        foreach (GameObject x in Levels)
        {
            x.SetActive(false);
        }
        Levels[current_Level].SetActive(true);

        if(Player != null)
        PF = Player.GetComponent<PlayerHelper>().pf;

        //Generic krna hai
        SliderDistanceCovered.maxValue = 95;

    }
    public void LevelUp()
    {
        
        if (current_Level < Levels.Length - 1)
        {
            current_Level++;
            PlayerPrefs.SetInt("Level", current_Level++);
            PlayerPrefs.Save();

            
            SceneManager.LoadScene(0);
            
        }
        else
        {
            PlayerPrefs.SetInt("Level", 0);
            PlayerPrefs.Save();

            SceneManager.LoadScene(0);
        }

        Admanager.instance.ShowInterstitialAd();
    }

}
