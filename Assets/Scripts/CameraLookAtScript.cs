﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAtScript : MonoBehaviour
{
    public Transform Target;
    public float t = 0.1f;
    public float LeastDistance = 0.2f;

    public Transform[] MovePosition;
    public int CurrentMovePosition;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Target.position);

        //Mathf.Lerp(,);
        Vector3 temp =  Vector3.Lerp(transform.position, MovePosition[CurrentMovePosition].position , t);
        transform.position = temp;

        if (Vector3.Distance(transform.position, MovePosition[CurrentMovePosition].position) < LeastDistance)
        {
            CurrentMovePosition++;

            if (CurrentMovePosition > MovePosition.Length - 1)
            {
                CurrentMovePosition = 0;
            }
        }
    }
}
