﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PathCreation.Examples;

public class PlayerHelper : MonoBehaviour
{
   
    public GameObject[] Cameras;
    public Ui_Controller UIS;
    public GameController GCS;
    public PathFollower pf;
    public bool canMove = true;
    public bool LevelComplete = false;
    
    
    // Start is called before the first frame update
    void Awake()
    {
        pf = gameObject.GetComponentInParent<PathFollower>();
        canMove = false;
        GCS.Player = gameObject;
        UIS.PHS = gameObject.GetComponent<PlayerHelper>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnStartRun()
    {
        if (LevelComplete)
            return;

        if(canMove==true)
        {
            gameObject.GetComponent<Animator>().SetBool("Walk", true);
            pf.speed = 5;

        }
       
    }
    public void OnStopRun()
    {
        gameObject.GetComponent<Animator>().SetBool("Walk", false);
        pf.speed = 0;

    }
    private void OnTriggerEnter(Collider other)
    {

        if(other.gameObject.CompareTag("Win_trigger"))
        {
            if (LevelComplete == true)
                return;

            gameObject.GetComponent<Animator>().SetBool("Dance", true);
            canMove = false;
            OnStopRun();
            GCS.SM.OnLevelComplete();
            Invoke("LevelMax", 5f);
            LevelComplete = true;
        }
      
        if (other.gameObject.CompareTag("Rotator_Rod"))
        {
            gameObject.GetComponent<Animator>().SetBool("Fail", true);
            OnStopRun();
            canMove = false;
            GCS.SM.OnLevelFail();
            Invoke("WaitforAnimation", 4f);

        }
        if(other.gameObject.CompareTag("Pendulum_Sphere"))
        {
            gameObject.GetComponent<Animator>().SetBool("Fail", true);
            canMove = false;
            GCS.SM.OnLevelFail();
            Invoke("WaitforAnimation", 4f);
        }
        if (other.gameObject.CompareTag("Rotating_ring"))
        {
            gameObject.GetComponent<Animator>().SetBool("Fail", true);
            canMove = false;
            GCS.SM.OnLevelFail();
            Invoke("WaitforAnimation", 4f);

        }
        if (other.gameObject.CompareTag("hammer"))
        {
            gameObject.GetComponent<Animator>().SetBool("Fail", true);
            canMove = false;
            GCS.SM.OnLevelFail();
            Invoke("WaitforAnimation", 4f);
        }
        if (other.gameObject.CompareTag("Road_remover"))
        {
            gameObject.GetComponent<Animator>().SetBool("Fail", true);
            canMove = false;
            GCS.SM.OnLevelFail();
            Invoke("WaitforAnimation", 4f);
        }
        if (other.gameObject.CompareTag("Boxing_punch"))
        {
            gameObject.GetComponent<Animator>().SetBool("Fail", true);
            canMove = false;
            GCS.SM.OnLevelFail();
            Invoke("WaitforAnimation", 4f);
        }

        if (other.gameObject.CompareTag("Camera1"))
        {
            foreach(GameObject x in Cameras)
            {
                x.SetActive(false);
            }
            Cameras[0].SetActive(true);
        }
        if(other.gameObject.CompareTag("Camera2"))
        {

            foreach (GameObject x in Cameras)
            {
                x.SetActive(false);
            }
            Cameras[1].SetActive(true);
        }
        if(other.gameObject.CompareTag("Camera3"))
        {

            foreach (GameObject x in Cameras)
            {
                x.SetActive(false);
            }
            Cameras[2].SetActive(true);
        }
        if(other.gameObject.CompareTag("Camera4"))
        {
            foreach (GameObject x in Cameras)
            {
                x.SetActive(false);
            }
            Cameras[3].SetActive(true);
        }
      

    }
    
    public void WaitforAnimation()
    {
        SceneManager.LoadScene(0);
    }
    public void LevelMax()
    {
        UIS.OnLevelCompleted();
    }
}
