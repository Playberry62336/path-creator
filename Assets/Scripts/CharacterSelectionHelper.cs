﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectionHelper : MonoBehaviour
{
    public Main_Menu MM;
    public int PlayerNumber;
    public Image PlayerImg;
    public GameObject Use;
    public GameObject ItemInUse;
    public GameObject BG;
    public GameObject BGHighlighted;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void InitData(Image img)
    {
        PlayerImg = img;
    }

    public void IsSelected()
    {
        BGHighlighted.SetActive(true);
        BG.SetActive(true);
        ItemInUse.SetActive(true);
        Use.SetActive(false);
    }
    public void IsNotSelected()
    {
        BGHighlighted.SetActive(false);
        BG.SetActive(false);
        ItemInUse.SetActive(false);
        Use.SetActive(true);
    }

    public void OnClick()
    {
       // MM.current_SelectedPlayer = PlayerNumber;
        MM.OnClickCharacterSelection(PlayerNumber);
        

       
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
