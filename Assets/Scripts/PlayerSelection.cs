﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelection : MonoBehaviour
{
    public GameObject[] Players;
 

    // Start is called before the first frame update
    void Start()
    {
        int SelectedPlayer = PlayerPrefs.GetInt("SelectdPlayer"); 

        foreach (GameObject x in Players)
        {
            x.SetActive(false);
        }
        Players[SelectedPlayer].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
