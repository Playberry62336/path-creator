﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdolController : MonoBehaviour
{
   
   // public List<Rigidbody> rbs;
   // public List<Collider> colliders;
    public Rigidbody[] rbs;
    public Collider[] colliders;
  
    // Start is called before the first frame update
    void Start()
    {
        rbs=gameObject.GetComponentsInChildren<Rigidbody>();
        colliders = gameObject.GetComponentsInChildren<Collider>();
    }
    public void EnableRagdoll()
    {
        foreach(Rigidbody x in rbs)
        {
            x.isKinematic = false;
            x.useGravity = true;
        }
        foreach(Collider x in colliders)
        {
            x.enabled = true;
        }
        
        GetComponent<Animator>().enabled = false;
    }
    public void DisableRagdoll()
    {
        foreach (Rigidbody x in rbs)
        {
            x.isKinematic = true;
            x.useGravity = false;
        }
        foreach (Collider x in colliders)
        {
            x.enabled = false;
        }
        
        GetComponent<Animator>().enabled = true;

    }
    // Update is called once per frame
    void Update()
    {
        
    }
    
}
