﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PlayerData
{
    public int ID;
    public Sprite PlayerImage;
    public bool isFree;
    public int Price;
    public GameObject Player;
//    public bool isUnlocked;
}


public class Main_Menu : MonoBehaviour
{
    public int current_Player;
    public int current_SelectdPlayer;
    public GameObject[] Players;


    public List<PlayerData> AvailablePlayers;
    public CharacterSelectionHelper[] CSH;
    // Start is called before the first frame update
    void Start()
    {
        InitPlayers();
    }

    private void InitPlayers()
    {
        int i = 1;
        foreach (PlayerData x in AvailablePlayers)
        {
            CSH[i].PlayerNumber = x.ID;
            CSH[i].PlayerImg.sprite = x.PlayerImage;


            if (i == 0)
            {
                CSH[i].IsSelected();
                x.Player.SetActive(true);
            }
            else
                CSH[i].IsNotSelected();

            i++;
        }
          


    }


    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnClickCharacterSelection(int i)
    {

        foreach (CharacterSelectionHelper x in CSH)
        {
            x.IsNotSelected();
        }

        CSH[i].IsSelected();
        AvailablePlayers[i].Player.SetActive(true);




        foreach (PlayerData x in AvailablePlayers)
        {
            x.Player.SetActive(false);
        }
        AvailablePlayers[i].Player.SetActive(true);
        PlayerPrefs.SetInt("SelectedPlayer", i);
        PlayerPrefs.Save();
    }
}
