﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Ui_Controller : MonoBehaviour
{
    public GameController GCS;
    public PlayerHelper PHS;
    public GameObject InGame_Panel;
    public GameObject Start_Panel;
    public GameObject LevelComplete_Panel;
    public GameObject Mute_Button;
    public GameObject Volume_Button;
    public GameObject Restore_Button;
    public GameObject Guidance_Button;
    public Text CountDown_text;
    public int coin = 0;
    public Text coin_text;
    
    // Start is called before the first frame update
    void Start()
    {
        int Amount = PlayerPrefs.GetInt("Coins");
        coin_text.text = Amount.ToString();


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnClickGuidance()
    {
        Guidance_Button.SetActive(false);
    }
    public void OnClickPlayerImage()
    {
        SceneManager.LoadScene(1);
    }
    public void OnClickStart()
    {
       
        Start_Panel.SetActive(true);
        InGame_Panel.SetActive(false);
        StartCoroutine("ShowCountDown");
    }
    public void OnClickVolumeButton()
    {
        Mute_Button.SetActive(true);
        Volume_Button.SetActive(false);
    }
    public void OnClickMuteButton()
    {
        Volume_Button.SetActive(true);
        Mute_Button.SetActive(false);
    }
    public void OnCilckSetting_Button()
    {
        Restore_Button.SetActive(true);
        Invoke("Restore", 3f);
    }
    public void Restore()
    {
        Restore_Button.SetActive(false);
    }
    public void OnClickRestoreButton()
    {
        PlayerPrefs.DeleteAll();
    }
    
    public void OnLevelCompleted()
    {
        coin = PlayerPrefs.GetInt("Coins") + 100;
        PlayerPrefs.SetInt("Coins", coin);
        PlayerPrefs.Save();
        LevelComplete_Panel.SetActive(true);

        int Amount = PlayerPrefs.GetInt("Coins");
        coin_text.text = Amount.ToString();


    } 
    public void OnClickLevelCompleted()
    {
        LevelComplete_Panel.SetActive(false);
        GCS.LevelUp();
    }
    IEnumerator ShowCountDown()
    {
        PHS.canMove = false;
        yield return new WaitForSecondsRealtime(1f);
        CountDown_text.text = "3";
        yield return new WaitForSecondsRealtime(1f);
        CountDown_text.text = "2";
        yield return new WaitForSecondsRealtime(1f);
        CountDown_text.text = "1";
        yield return new WaitForSecondsRealtime(1f);
        CountDown_text.text = "Go";
      
        yield return new WaitForSecondsRealtime(1f);
        CountDown_text.text = "";

        PHS.canMove = true;

    }
}
